msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-15 04:49+0200\n"
"PO-Revision-Date: 2023-05-17 03:12+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/censorship-no/"
"public-private/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.18-dev\n"

#
#: concepts/public-private.md:block 1 (header)
#, fuzzy
#| msgid "Public vs. private browsing"
msgid "Public vs. Personal browsing"
msgstr "مقایسه‌ی مرور عمومی و مرور خصوصی"

#: concepts/public-private.md:block 2 (paragraph)
msgid ""
"Because of the many techniques used to overcome connectivity issues, Ceno "
"may become a convenient way for you to get all kinds of Web content. And, as "
"you may have already read in previous sections, whenever you retrieve and "
"seed a page using Ceno Browser, it becomes available to others. There may be "
"some content, however, that you do not wish to share (or you do not want to "
"let others know that you are trying to or did retrieve), and fortunately "
"Ceno can help you in this instance as well."
msgstr ""
"به دلیل تکنیک‌های بسیاری که برای غلبه بر مشکلات اتصال به کار رفته‌اند، Ceno "
"می‌تواند برای شما به راه مناسبی برای دریافت هرگونه محتوای وب بدل شود. و "
"همان‌طور که شاید پیشاپیش در بخش‌های پیشین خوانده‌اید، هرگاه با استفاده از سنو "
"صفحه‌ای را به کمک مرورگر سنو بازیابی و بذرپاشی کنید، این صفحه در دسترس دیگران "
"نیز قرار می‌گیرد. با این حال، شاید محتوایی در میان باشد که مایل نباشید آن را "
"به اشتراک بگذارید (یا شاید شما نمی‌خواهید به دیگران اجازه دهید بدانند که شما "
"سعی می‌کنید آن را بازیابی کنید یا پیشاپیش آن را بازیابی کرده‌اید)، و خوشبختانه "
"سنو در این مورد نیز می‌تواند به شما کمک کند."

#: concepts/public-private.md:block 3 (paragraph)
#, fuzzy
#| msgid ""
#| "The default mode when you launch the application is **public browsing**. "
#| "In it, Ceno accesses Web content as described previously:"
msgid ""
"The default mode when you launch the application is **Public browsing**. In "
"it, Ceno accesses Web content as described previously:"
msgstr ""
"حالت پیش‌فرض وقتی شما نرم‌افزار را اجرا می‌کنید **مرور عمومی** است. در این "
"حالت، Ceno به محتوای وب همان‌گونه دسترسی می‌یابد که پیش‌تر وصف کردیم:"

#: concepts/public-private.md:block 4 (ordered list)
msgid "Direct access is attempted."
msgstr "تلاش برای دسترسی مستقیم صورت گرفته است."

#: concepts/public-private.md:block 4 (ordered list)
msgid "Failing that, the distributed cache is searched."
msgstr "در صورت ناکامی در آن، کش توزیع‌شده جست‌وجو می‌شود."

#: concepts/public-private.md:block 4 (ordered list)
msgid ""
"Failing that, the content is requested via an injector (maybe via another "
"client)."
msgstr ""
"در صورت ناکامی در آن، محتوا از طریق یک injector (شاید از طریق یک "
"سرویس‌گیرنده‌ی دیگر) درخواست می‌شود."

#: concepts/public-private.md:block 5 (paragraph)
#, fuzzy
#| msgid ""
#| "Ceno also has a **private browsing** mode. In it, the distributed cache "
#| "is never searched, and injection is never attempted:"
msgid ""
"Ceno also has a **Personal browsing** mode. In it, the distributed cache is "
"never searched, and injection is never attempted:"
msgstr ""
"مرورگر Ceno یک حالت **مرور خصوصی** نیز دارد. در این حالت، کش توزیع‌شده هرگز "
"جست‌وجو نمی‌شود، و تلاش برای Injection هرگز صورت نمی‌پذیرد:"

#: concepts/public-private.md:block 6 (ordered list)
msgid ""
"Failing that, an injector is contacted (maybe via another client) and used "
"*as a normal proxy server*. Note that in this case, neither the injector nor "
"your client updates the distributed cache with your page."
msgstr ""
"در صورت ناکامی در آن، با یک injector تماس گرفته می‌شود (شاید از طریق یک "
"سرویس‌گیرنده‌ی دیگر) و **به‌عنوان یک سرور پروکسی معمولی** به کار می‌رود. دقت "
"کنید که در این مورد، نه injector و نه سرویس‌گیرنده‌ی شما کش توزیع‌شده‌ی شما را "
"با صفحه‌ی شما به‌روز نمی‌کنند."

#: concepts/public-private.md:block 7 (paragraph)
#, fuzzy
#| msgid ""
#| "The different behavior results in different characteristics. Thus, in "
#| "public mode:"
msgid ""
"The different behavior results in different characteristics. Thus, in Public "
"mode:"
msgstr "رفتار متفاوت به ویژگی‌های متفاوتی می‌انجامد. بنابراین، در حالت عمومی:"

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"You have better chances to get Web content, and help others get that content "
"(from you)."
msgstr ""
"شما شانس‌های بهتری برای دریافت محتوای وب دارید، و به دیگران کمک می‌کنید تا آن "
"محتوا را (از شما) دریافت کنند."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Pages with dynamic content (e.g. updated in real time) may break in obvious "
"or subtle ways."
msgstr ""
"صفحه‌های دارای محتوای پویا (مثلاً به‌روزشده در زمان واقعی) ممکن است به طرزی "
"آشکار یا ظریف تکه‌تکه شوند."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Pages requiring authentication do not work (as passwords and cookies are "
"removed by the client)."
msgstr ""
"صفحه‌‌هایی که نیازمند هویت‌سنجی‌اند کار نمی‌کنند (چون رمزهای عبور و کوکی‌ها از سوی "
"سرویس‌گیرنده حذف شده‌اند)."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Some browsing activity may be leaked to other users (see [risks](risks.md))."
msgstr ""
"بعضی از فعالیت‌های مرورگری ممکن است به کاربران دیگر درز کنند ( [خطرات](risks."
"md))."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Some browsing activity may be leaked to injectors (see [risks](risks.md))."
msgstr ""
"بعضی از فعالیت‌های مرورگری ممکن است بهinjectorها درز کنند ([خطرات](risks.md))."

#: concepts/public-private.md:block 8 (ordered list)
msgid "You need to trust injectors to retrieve and sign Web content."
msgstr "شما برای بازیابی و امضای محتوای وب بایستی به injectorها اعتماد کنید."

#: concepts/public-private.md:block 9 (paragraph)
#, fuzzy
#| msgid "While in private mode:"
msgid "While in Personal mode:"
msgstr "در حالت مرور خصوصی:"

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"You may not be able to access blocked Web content if international "
"connectivity is too scarce; even if you could, other Ceno users would not "
"get that content from you."
msgstr ""
"اگر اتصال بین‌المللی خیلی ضعیف باشد ممکن است قادر به دسترسی به محتوای وب "
"مسدودشده نباشید؛ حتی اگر شما بتوانید به آن دسترسی پیدا کنید، سایر کاربران "
"Ceno آن محتوا را از شما دریافت نخواهند کرد."

#: concepts/public-private.md:block 10 (ordered list)
msgid "Pages with dynamic content will probably work."
msgstr "صفحه‌های دارای محتوای پویا احتمالاً کار خواهند کرد."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"Pages requiring authentication may work (when your connection is protected "
"by HTTPS, the injector does not see your passwords)."
msgstr ""
"صفحه‌های نیازمند به هویت‌سنجی شاید کار کنند (وقتی اتصال شما تحت حفاظت HTTPS "
"باشد، injector رمزهای عبورتان را نمی‌بیند)."

#: concepts/public-private.md:block 10 (ordered list)
msgid "Browsing activity is not leaked to other users."
msgstr "فعالیت مرورگری به کاربران دیگر درز نمی‌کند."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"Limited browsing activity is leaked to injectors (with HTTPS, only the "
"origin server name or address)."
msgstr ""
"فعالیت مرورگری تا اندازه‌ی محدودی به injectorها درز می‌کند (با HTTPS، فقط نام "
"یا نشانی سرور اصلی)."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"You need not trust injectors (with HTTPS, usual certificate-based security "
"still works)."
msgstr ""
"نیازی نیست بهinjectorها اعتماد کنید (با HTTPS، امنیت گواهی‌محور معمول همچنان "
"کار می‌کند)."

#: concepts/public-private.md:block 11 (paragraph)
#, fuzzy
#| msgid ""
#| "In conclusion: if you are using Ceno to read the news, watch videos, "
#| "browse Wikipedia and other static resources that are otherwise censored "
#| "in your network, consider using the default *public browsing* mode. And "
#| "if you want to login to Twitter or edit your WordPress website, use "
#| "*private browsing* mode."
msgid ""
"In conclusion: if you are using Ceno to read the news, watch videos, browse "
"Wikipedia and other static resources that are otherwise censored in your "
"network, consider using the default *Public browsing* mode. And if you want "
"to login to Twitter or edit your WordPress website, use *Personal browsing* "
"mode."
msgstr ""
"در نتیجه: اگر برای خواندن اخبار، تماشای ویدئوها، مرور ویکی‌پدیا و سایر مراجع "
"ایستایی از Ceno استفاده می‌کنید که در غیر این صورت در شبکه‌ی شما سانسور "
"شده‌اند، استفاده از حالت پیش‌فرض **مرور عمومی** را لحاظ کنید. و اگر می‌خواهید "
"وارد حساب کاربری توییترتان شوید یا وب‌سایت وردپرس خودتان را ویرایش کنید، از "
"حالت **مرور خصوصی** استفاده کنید."

#: concepts/public-private.md:block 12 (paragraph)
#, fuzzy
#| msgid ""
#| "Please read the section on [risks](risks.md) for a more detailed "
#| "explanation. Also note that your client can continue to operate as a "
#| "bridge and a seeder regardless of public or private browsing. We explain "
#| "this in greater detail in the [Helping others](../browser/bridging.md) "
#| "section of the manual."
msgid ""
"Please read the section on [risks](risks.md) for a more detailed "
"explanation. Also note that your client can continue to operate as a bridge "
"and a seeder regardless of Public or Personal browsing. We explain this in "
"greater detail in the [Helping others](../browser/bridging.md) section of "
"the manual."
msgstr ""
"برای توضیحی جزئی‌تر لطفاً بخش مربوط به [خطرات](risks.md) را بخوانید. همچنین به "
"یاد داشته باشید که سرویس‌گیرنده‌ی شما می‌تواند به عملکردش به‌عنوان یک پل و یک "
"بذرپاش، صرف‌نظر از مرور عمومی یا خصوصی، ادامه دهد. ما این نکته را در بخش [کمک "
"به دیگران](../browser/bridging.md) از دفترچه‌ی راهنما به طور مفصل‌تر توضیح "
"می‌دهیم."
