#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-09-05 22:01+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Urdu <https://hosted.weblate.org/projects/censorship-no/"
"homepage-readme/ur/>\n"
"Language: ur\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: README.md:block 1 (header)
msgid "Censorship.no! User Manual"
msgstr "Censorship.no! یوزر مینوئل"

#: README.md:block 2 (paragraph)
msgid ""
"This manual is aimed at users of [Ceno Browser][Ceno Browser] and related "
"technologies, as created for the [Censorship.no!][Censorship.no!] project by "
"[eQualitie][eQualitie]."
msgstr ""
"اس ہدایت نامہ کا مقصد [Ceno براؤزر][Ceno Browser] اور متعلقہ ٹیکنالوجیز کے "
"صارفین کے لیے ہے، جیسا کہ [eQualitie][eQualitie] کے [Censorship.no!]"
"[Censorship.no!] پروجیکٹ کے لیے بنایا گیا ہے۔"

#: README.md:block 3 (paragraph)
msgid ""
"If you are looking for technical documentation, please refer to the [Ceno "
"documentation repository][Ceno documentation repository], where you will "
"find pointers for further reading and details on implementation, as well as "
"the [protocol specifications][protocol specifications]."
msgstr ""
"اگر آپ تکنیکی دستاویزات تلاش کر رہے ہیں، تو براہ کرم [Ceno دستاویزات کے "
"ذخیرے][Ceno دستاویزات کے ذخیرے] سے رجوع کریں، جہاں آپ کو مزید پڑھنے اور نفاذ "
"کے بارے میں تفصیلات کے ساتھ [پروٹوکول وضاحتیں][پروٹوکول وضاحتیں] ملیں گے۔"

#: README.md:block 3 (paragraph)
msgid "[Ceno Browser]: https://gitlab.com/censorship-no/ceno-browser"
msgstr "[Ceno Browser]: https://gitlab.com/censorship-no/ceno-browser"

#: README.md:block 3 (paragraph)
msgid "[Censorship.no!]: https://censorship.no/"
msgstr "[Censorship.no!]: https://censorship.no/"

#: README.md:block 3 (paragraph)
msgid "[eQualitie]: https://equalit.ie/"
msgstr "[eQualitie]: https://equalit.ie/"

#: README.md:block 3 (paragraph)
msgid ""
"[Ceno documentation repository]: https://gitlab.com/censorship-no/ceno-docs/"
msgstr "[Ceno دستاویزات کا ذخیرہ]: https://gitlab.com/censorship-no/ceno-docs/"

#: README.md:block 3 (paragraph)
msgid ""
"[protocol specifications]: https://gitlab.com/equalitie/ouinet/blob/master/"
"doc/ouinet-network-whitepaper.md"
msgstr ""
"[پروٹوکول تفصیلات]: https://gitlab.com/equalitie/ouinet/blob/master/doc/"
"ouinet-network-whitepaper.md"
