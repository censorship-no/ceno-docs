#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-09-05 22:50+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/censorship-no/"
"browser-bridging/pt/>\n"
"Language: pt\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: browser/bridging.md:block 1 (header)
msgid "Helping other Ceno users browse the Web"
msgstr "Ajudar outros utilizadores Ceno a navegar a Web"

#: browser/bridging.md:block 2 (paragraph)
msgid ""
"A peer-to-peer network is built from every node connected to it (yes, that "
"means you as well!). The more nodes, the stronger and more versatile the "
"network becomes. If you are running Ceno Browser from a country that does "
"not censor the Internet (or not as heavily as some), consider helping other "
"Ceno users by becoming a **bridge** node. You will then begin to route "
"traffic between clients living in heavily censored countries and Ceno "
"injectors. You will not be able to see their traffic (it will be sent "
"through an encrypted tunnel), nor will any of this traffic remain on your "
"device."
msgstr ""
"Uma rede P2P é construída a partir de cada nó lhe conectado (sim, isso "
"significa, também!). Quanto mais nós, mais forte e mais versátil a rede se "
"torna. Se estiver executando o Navegador Ceno de um país que não censura a "
"Internet (ou não o faz tão incisivamente quanto alguns), considere ajudar "
"outros utilizadores do Ceno se tornando um nó **ponte**. Começará, então, a "
"encaminhar tráfego entre clientes vivendo em países fortemente censurados e "
"injetores do Ceno. Não conseguirá ver o tráfego deles (este será enviado "
"através de um túnel criptografado), tampouco haverá no seu aparelho qualquer "
"registo remanescente deste tráfego."

#: browser/bridging.md:block 3 (quote)
msgid ""
"**Note:** The configuration described in this section may also help your "
"device to effectively seed content to others on the distributed cache, so "
"please consider applying it as well when using Ceno in a censoring country "
"(but keep in mind the [risks](../concepts/risks.md) of serving such content "
"to others)."
msgstr ""
"**Nota:** A configuração descrita nesta secção pode também ajudar o seu "
"aparelho a semear com eficiência conteúdo para outros no cache distribuído. "
"Assim, por favor, considere aplicá-la igualmente quando usar o Ceno num país "
"censurador (mas tenha em mente os [riscos](../concepts/risks.md)) de "
"oferecer tal conteúdo para outros)."

#: browser/bridging.md:block 4 (header)
msgid "How to become a Ceno bridge"
msgstr "Como se tornar uma ponte do Ceno"

#: browser/bridging.md:block 5 (paragraph)
msgid ""
"This functionality is already built into Ceno Browser. Your device will need "
"to be connected to a Wi-Fi network that has either UPnP enabled or explicit "
"port forwarding configured for Ceno. See the next sections for further "
"details."
msgstr ""
"Esta funcionalidade já esta embutida no Navegador Ceno. O seu aparelho "
"precisará ser conectado a uma rede WiFi que tenha ou UPnP ativado ou "
"redirecionamento de porta explicitamente configurado para o Ceno. Veja as "
"próximas secções para maiores detalhes."

#: browser/bridging.md:block 6 (paragraph)
msgid ""
"However, please note that Android will only allow a mobile device to act as "
"a proper bridge while you are actively using it, as power-saving features "
"will throttle the operation of Ceno otherwise."
msgstr ""
"No entanto, por favor observe que o Android só permitirá ao aparelho móvel "
"agir como uma porta adequada enquanto o usar ativamente. Do contrário, as "
"funcionalidades de economia de energia limitarão as operações do Ceno."

#: browser/bridging.md:block 7 (quote)
msgid ""
"**Technical note:** This is mainly due to Android's [Doze mode](https://"
"developer.android.com/training/monitoring-device-state/doze-standby "
"\"Android Developers – Optimize for Doze and App Standby\") slowing down the "
"operation of the native Ouinet library. Unfortunately, disabling battery "
"optimization for Ceno does not seem to exclude Ouinet from it. Your "
"particular device may also include its own power-saving features which may "
"interfere with Ceno; please check [Don't kill my app!][Don't kill my app!] "
"for your device's brand."
msgstr ""
"**Nota técnica:** Isso se deve principalmente ao [Modo soneca](https://"
"developer.android.com/training/monitoring-device-state/doze-standby "
"\"Android Developers – Optimize for Doze and App Standby\") do Android, o "
"qual retarda as operações da biblioteca nativa Ouinet. Infelizmente, "
"desativar a otimização de pilha para o Ceno parece não excluir a Ouinet da "
"ação. O seu aparelho específico pode também incluir as suas próprias "
"funcionalidades de economia de energia, o que pode interferir no "
"funcionamento do Ceno. Por favor, consulte o website [Don't kill my app!]"
"[Don't kill my app!] para informações sobre a marca do seu aparelho."

#: browser/bridging.md:block 8 (paragraph)
msgid ""
"Thus if you intend to have Ceno acting as a permanent, always-reachable "
"bridge, besides a properly configured Wi-Fi network you will need to:"
msgstr ""
"Portanto, se pretende manter o Ceno agindo como uma ponte permanente, sempre "
"alcançável, além de uma rede WiFi configurada apropriadamente, precisará:"

#: browser/bridging.md:block 9 (ordered list)
msgid "Have your device plugged in to power at all times."
msgstr "Ter o seu aparelho ligado à rede elétrica a todo tempo."

#: browser/bridging.md:block 9 (ordered list)
msgid "Have the device's screen on at all times."
msgstr "Ter o ecrã do aparelho ativo a todo tempo."

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"One convenient way of doing this without much power consumption and "
"obnoxious, permanent lighting is using Android's screen saver: enable it "
"under *Settings / Display / Screen saver* (or *Daydream* in some versions), "
"pick the *Clock* widget, choose *When to start screen saver* in the menu and "
"select *While charging* or *Either*. A very dimmed down clock will appear on "
"a black background while the device is not active."
msgstr ""
"Uma forma conveniente de fazer isso sem muito consumo de energia ou a "
"incômoda iluminação permanente é usar o protetor do ecrã do Android: ative-o "
"em *Configurações/ Visor / Protetor do ecrã* (ou *Daydream* em algumas "
"versões), abra o widget *Relógio*, escolha *Quando iniciar a proteção do "
"ecrã* no menu e selecione *Ao carregar* ou *Ambos*. Um relógio muito "
"atenuado aparecerá num fundo preto enquanto o aparelho não estiver ativo."

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"Please note that you should not use the power button to lock the device as "
"this will turn the screen off. Instead, just wait for the device to lock "
"itself with the screen on."
msgstr ""
"Por favor, note que não deve usar botão de desligar para travar o aparelho, "
"pois essa ação desligará o ecrã. Ao invés disso, apenas mantenha a ecrã "
"ligado e aguarde até que o aparelho se bloqueie."

#: browser/bridging.md:block 10 (paragraph)
msgid ""
"If that setup is not an option for you, do not desist yet! If you have a "
"computer with good connectivity that stays on most of the time, please "
"continue reading."
msgstr ""
"Se essa configuração não é possível para si, não desista ainda! Se possuir "
"um computador com uma boa conectividade que fique ligado a maior parte do "
"tempo, por favor continue lendo."

#: browser/bridging.md:block 11 (header)
msgid "Running a bridge on a computer"
msgstr "Executando uma ponte num computador"

#: browser/bridging.md:block 12 (paragraph)
msgid ""
"If your computer supports [Docker containers][docker], you can run a pre-"
"configured Ceno client on it to act as a bridge. If Docker is not yet "
"installed, please follow the instructions to [install the Docker Engine]"
"[docker-install] in your platform. For Debian derivatives like Ubuntu or "
"Linux Mint, you can just run: `sudo apt install docker.io`"
msgstr ""
"Se o seu computador suporta [contentores do Docker][docker], pode executar "
"nele um cliente pré-configurado do Ceno para atuar como uma ponte. Se o "
"Docker não estiver instalado ainda, por favor, siga as instruções para "
"[instalar o Docker Engine][docker-install] na sua plataforma. Para derivados "
"do Debian como Ubuntu ou Linux Mint, pode apenas executar: `sudo apt install "
"docker.io`"

#: browser/bridging.md:block 13 (paragraph)
msgid ""
"To deploy a Ceno client container you only need to run the following command "
"on a terminal (it looks scary but you can just copy and paste it as is on "
"the command line):"
msgstr ""
"Para implantar um contentor de cliente Ceno apenas precisa executar os "
"seguintes comandos num terminal (parece assustador, mas pode simplesmente "
"copiar e colar na linha de comando):"

#: browser/bridging.md:block 15 (paragraph)
msgid ""
"If your computer is not based on GNU/Linux, the command needs to be slightly "
"different:"
msgstr ""
"Se o seu computador não é baseado em GNU/Linux, o comando precisa ser "
"ligeiramente diferente:"

#: browser/bridging.md:block 17 (paragraph)
msgid ""
"The command will start a container named `ceno-client` that will run on "
"every boot unless you explicitly tell it to stop. Please check the [Ceno "
"Docker client documentation][ceno-client-doc] for more information on how to "
"manipulate the container."
msgstr ""
"O comando iniciará um contentor chamado `ceno-client`, que será executado a "
"cada inicialização a menos que explicitamente lhe diga para parar. Por "
"favor, verifique a [Documentação do cliente Docker do Ceno][ceno-client-doc] "
"para mais informações sobre como manipular o contentor."

#: browser/bridging.md:block 18 (quote)
msgid ""
"**Note:** This client has no *Ceno Settings*: when instructed below to "
"access that page, open instead the [client front-end](../client/front-end."
"md), which contains mostly equivalent information."
msgstr ""
"**Nota:** Este cliente não possui *Configurações do Ceno*: quando indicarmos "
"nas instruções abaixo para aceder essa página, ao invés disso abra a [front-"
"end do cliente](../client/front-end.md), que contém na sua maioria "
"informações equivalentes."

#: browser/bridging.md:block 19 (header)
msgid "Enabling UPnP on your Wi-Fi router"
msgstr "Ativando UPnP no seu roteador WiFi"

#: browser/bridging.md:block 20 (paragraph)
msgid ""
"[UPnP][UPnP] is the easiest way of making your Ceno Browser (or computer "
"client) reachable to the Ceno network. The [Ceno Settings](settings.md) page "
"will indicate the UPnP status on your local network."
msgstr ""
"[UPnP][UPnP] é a maneira mais fácil de tornar o seu Navegador Ceno (ou "
"cliente de computador) alcançável à rede Ceno. A página [Configurações do "
"Ceno](settings.md) irá indicar o estado do UPnP na sua rede local."

#: browser/bridging.md:block 21 (quote)
msgid ""
"**Note:** Enabling UPnP on the Wi-Fi router may expose devices on your "
"network to external interference. Please make yourself [aware of the risks]"
"[upnp-risks] and also consider using alternative methods as explained below."
msgstr ""
"**Nota:** Ativar UPnP no roteador WiFi pode expor aparelhos na sua rede à "
"interferência externa. Por favor, esteja [ciente dos riscos][upnp-risks] e "
"considere também métodos alternativos, como explicado abaixo."

#: browser/bridging.md:block 22 (paragraph)
msgid ""
"A status like the one below indicates that UPnP is not available or not "
"working on your WiFi router:"
msgstr ""
"Um estado como o mostrado abaixo indica que UPnP não está disponível ou não "
"funciona no seu roteador WiFi:"

#: browser/bridging.md:block 23 (quote)
msgid "**Reachability status**"
msgstr "**Estado de acessibilidade**"

#: browser/bridging.md:block 23 (quote)
msgid "**UPnP status**"
msgstr "**Estado do UPnP**"

#: browser/bridging.md:block 24 (paragraph)
msgid ""
"The status below indicates that UPnP is likely working and Ceno is currently "
"verifying connectivity:"
msgstr ""
"O estado abaixo indica que o UPnP provavelmente funciona e que o Ceno, "
"verifica a conectividade no momento:"

#: browser/bridging.md:block 26 (paragraph)
msgid ""
"The status below indicates that UPnP is working and you can bridge "
"connections for other Ceno users:"
msgstr ""
"O estado abaixo indica que o UPnP funciona e que pode ser ponte para "
"conexões de outros utilizadores do Ceno:"

#: browser/bridging.md:block 28 (quote)
msgid ""
"**Note:** Even if UPnP is working, your router may still not be reachable "
"from the outside. This can be the case when *Ceno Settings* reports "
"*External UDP endpoints* which look like [CGNAT][CGNAT] addresses `100.X.Y.Z:"
"N` with X between 64 and 127 (increasingly common among home ISPs), or like "
"private addresses `10.X.Y.Z:N`, `172.X.Y.Z:N` with X between 16 and 31, and "
"`192.168.X.Y:N`. If so, please contact your ISP or network administrator to "
"get a public address on your router or to establish port forwardings to the "
"external endpoint."
msgstr ""
"**Nota:** Mesmo quando UPnP funciona, o seu roteador pode não ser alcançável "
"do exterior. Esse pode ser o caso quando as *Configurações do Ceno* relatam "
"*Endpoints UDP externos* que parecem com endereços [CGNAT][CGNAT] `100.X.Y.Z:"
"N` com X entre 64 e 127 (cada dia mais comum entre ISPs domésticos), ou como "
"endereços privados `10.X.Y.Z:N`, `172.X.Y.Z:N` com X entre 16 e 31 e "
"`192.168.X.Y:N`. Se for o caso, por favor contate o seu ISP ou administrador "
"de rede para obter o endereço público no seu roteador ou para estabelecer "
"redirecionamentos de porta para o endpoint externo."

#: browser/bridging.md:block 29 (paragraph)
msgid ""
"There are many Wi-Fi routers on the market and each has their own particular "
"features. Herein a list of some manufacturers' instructions for enabling "
"UPnP:"
msgstr ""
"Há muitos roteadores WiFi no mercado e cada um tem as suas características "
"específicas. Aqui está uma lista das instruções de alguns fabricantes para "
"ativar o UPnP:"

#: browser/bridging.md:block 30 (unordered list)
msgid "[Linksys](https://www.linksys.com/us/support-article?articleNum=138290)"
msgstr ""
"[Linksys](https://www.linksys.com/us/support-article?articleNum=138290)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[D-Link](https://eu.dlink.com/uk/en/support/faq/routers/wired-routers/di-"
"series/how-do-i-enable-upnp-on-my-router)"
msgstr ""
"[D-Link](https://eu.dlink.com/uk/en/support/faq/routers/wired-routers/di-"
"series/how-do-i-enable-upnp-on-my-router)"

#: browser/bridging.md:block 30 (unordered list)
msgid "[Huawei](https://consumer.huawei.com/ph/support/content/en-us00275342/)"
msgstr ""
"[Huawei](https://consumer.huawei.com/ph/support/content/en-us00275342/)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Xfinity](https://www.xfinity.com/support/articles/configure-device-"
"discovery-for-wifi)"
msgstr ""
"[Xfinity](https://www.xfinity.com/support/articles/configure-device-"
"discovery-for-wifi)"

#: browser/bridging.md:block 30 (unordered list)
msgid "[TP-Link](https://community.tp-link.com/us/home/kb/detail/348)"
msgstr "[TP-Link](https://community.tp-link.com/us/home/kb/detail/348)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Netgear](https://kb.netgear.com/24306/How-do-I-enable-Universal-Plug-and-"
"Play-on-my-Nighthawk-router)"
msgstr ""
"[Netgear](https://kb.netgear.com/24306/How-do-I-enable-Universal-Plug-and-"
"Play-on-my-Nighthawk-router)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Ubiquiti](https://www.geekzone.co.nz/forums.asp?"
"forumid=66&topicid=205740&page_no=5#1725168)"
msgstr ""
"[Ubiquiti](https://www.geekzone.co.nz/forums.asp?"
"forumid=66&topicid=205740&page_no=5#1725168)"

#: browser/bridging.md:block 31 (header)
msgid "Using port forwarding as an alternative to UPnP"
msgstr "Usando o redirecionamento de porta como uma alternativa ao UPnP"

#: browser/bridging.md:block 32 (paragraph)
msgid ""
"Instead of enabling UPnP on your router, you can create a port forwarding "
"rule to make sure that connections from the Ceno network are forwarded to "
"your device. You will need to login to the router's administrative interface "
"and locate the *port forwarding* option. To see which IP address you need to "
"forward the connections to and the relevant port, open the *Ceno Settings* "
"page and look under the *Local UDP endpoints*."
msgstr ""
"Em vez de ativar o UPnP no seu roteador, pode criar uma regra de "
"redirecionamento de porta para ter certeza de que as conexões da rede Ceno "
"são encaminhadas para o seu aparelho. Precisará fazer login na interface "
"administrativa do roteador e localizar a opção *redirecionamento de porta*. "
"Para ver para qual endereço de IP precisa redirecionar as conexões e a porta "
"relevante, abra a página *Configurações do Ceno* e veja a secção *Endpoints "
"UDP locais*."

#: browser/bridging.md:block 33 (quote)
msgid "**Local UDP endpoints**"
msgstr "**Endpoints UDP locais**"

#: browser/bridging.md:block 34 (paragraph)
msgid ""
"The port forwarding must be for the UDP protocol (not TCP). Ceno chooses a "
"random port on first run and keeps it for subsequent runs, but your device's "
"local network IP address may change from time to time. Thus you should "
"periodically review the *Ceno Settings* page to see that your device is "
"reachable to the Ceno network."
msgstr ""
"O redirecionamento de porta deve ser para o protocolo UDP (não TCP). O Ceno "
"escolhe uma porta randômica na primeira execução e mantém tal escolha para "
"as execuções subsequentes. Mas o endereço de IP da rede local do seu "
"aparelho pode mudar de tempos em tempos. Por isso, deve periodicamente rever "
"a página *Configurações do Ceno* para confirmar se o seu aparelho está "
"alcançável pela rede Ceno."

#: browser/bridging.md:block 35 (quote)
msgid ""
"**Technical note:** Alternatively, you can make sure that the router always "
"assigns the same IP address to your device (e.g. via a static DHCP lease for "
"the device's MAC address)."
msgstr ""
"**Nota técnica:** Alternativamente, pode garantir que o roteador sempre "
"destine o mesmo endereço de IP para o seu aparelho (por exemplo, via "
"assinatura DHCP estática para o endereço MAC do aparelho)."

#: browser/bridging.md:block 35 (quote)
msgid ""
"[Doze mode]: https://developer.android.com/training/monitoring-device-state/"
"doze-standby"
msgstr ""
"[Modo soneca]: https://developer.android.com/training/monitoring-device-"
"state/doze-standby"

#: browser/bridging.md:block 35 (quote)
msgid "[Don't kill my app!]: https://dontkillmyapp.com/"
msgstr "[Don't kill my app!]: https://dontkillmyapp.com/"

#: browser/bridging.md:block 35 (quote)
msgid "[docker]: https://en.wikipedia.org/wiki/Docker_(software)"
msgstr "[docker]: https://pt.wikipedia.org/wiki/Docker_(software)"

#: browser/bridging.md:block 35 (quote)
msgid "[docker-install]: https://docs.docker.com/engine/install/"
msgstr "[docker-install]: https://docs.docker.com/engine/install/"

#: browser/bridging.md:block 35 (quote)
msgid ""
"[ceno-client-doc]: https://gitlab.com/censorship-no/ceno-docker-"
"client#running-the-client"
msgstr ""
"[ceno-client-doc]: https://gitlab.com/censorship-no/ceno-docker-client"
"#running-the-client"

#: browser/bridging.md:block 35 (quote)
msgid "[UPnP]: https://en.wikipedia.org/wiki/Universal_Plug_and_Play"
msgstr "[UPnP]: https://pt.wikipedia.org/wiki/Universal_Plug_and_Play"

#: browser/bridging.md:block 35 (quote)
msgid ""
"[upnp-risks]: https://www.howtogeek.com/122487/htg-explains-is-upnp-a-"
"security-risk"
msgstr ""
"[upnp-risks]: https://www.howtogeek.com/122487/htg-explains-is-upnp-a-"
"security-risk"

#: browser/bridging.md:block 35 (quote)
msgid "[CGNAT]: https://en.wikipedia.org/wiki/Carrier-grade_NAT"
msgstr "[CGNAT]: https://pt.wikipedia.org/wiki/Carrier_Grade_NAT"
