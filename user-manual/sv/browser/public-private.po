#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-05-17 03:02+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/censorship-no/"
"browser-public-private/sv/>\n"
"Language: sv\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18-dev\n"

#: browser/public-private.md:block 1 (header)
msgid "Public and Personal browsing modes"
msgstr ""

#: browser/public-private.md:block 2 (paragraph)
#, fuzzy
#| msgid ""
#| "As described [in a previous section](../concepts/public-private.md), Ceno "
#| "has two different modes of operation depending on whether you want to "
#| "share the content that you browse with others (public browsing) or not "
#| "(private browsing)."
msgid ""
"As described [in a previous section](../concepts/public-personal.md), Ceno "
"has two different modes of operation depending on whether you want to share "
"the content that you browse with others (Public browsing) or not (Personal "
"browsing)."
msgstr ""
"Som beskrivits [i ett tidigare avsnitt](.. /concepts/public-private.md) har "
"Ceno två olika driftsätt beroende på om du vill dela innehållet som du "
"surfar med andra (offentlig surfning) eller inte (privat surfning)."

#: browser/public-private.md:block 3 (paragraph)
msgid ""
"This setting applies *to each tab* that you open in the browser, i.e. you "
"can have Public browsing tabs and Personal browsing tabs. Ceno's default "
"whenever you start it or open a new tab (via the 'Public Tabs' option on the "
"homepage or the Tabs icon next to the Clear icon in the address bar) is to "
"use Public browsing. To open a new tab in Personal browsing mode, just "
"choose *Personal Tabs* on the homepage or switch to the Personal mode icon "
"in the Tabs menu."
msgstr ""

#: browser/public-private.md:block 4 (paragraph)
#, fuzzy
#| msgid ""
#| "You can tell public tabs from private ones because public tabs have a "
#| "lighter (or white) tool bar:"
msgid ""
"You can tell public tabs from personal ones because public tabs have a "
"lighter (or white) tool bar:"
msgstr ""
"Du kan särskilja publika flikar från privata eftersom att publika flikar har "
"ett lättare (eller vitt) verktygsfält:"

#: browser/public-private.md:block 5 (paragraph)
#, fuzzy
#| msgid "![Figure: A public browsing tab](images/public-tab.png)"
msgid "![Figure: A Public browsing tab](images/public-tab.png)"
msgstr "![Figur: En publik flik för surfning] (images/public-tab.png)"

#: browser/public-private.md:block 6 (paragraph)
msgid "In contrast, private tabs have a darker tool bar:"
msgstr "I kontrast så har privata flikar ett mörkare verktygsfält:"

#: browser/public-private.md:block 7 (paragraph)
#, fuzzy
#| msgid "![Figure: A private browsing tab](images/private-tab.png)"
msgid "![Figure: A Personal browsing tab](images/private-tab.png)"
msgstr "![Figur: En flik för privat surfning](images/private-tab.png)"

#: browser/public-private.md:block 8 (paragraph)
msgid ""
"Once you have loaded a page in a tab, the Ceno icon in the address bar will "
"help you know how it actually retrieved the different elements of the "
"content. We will cover this icon later on."
msgstr ""

#~ msgid "Using public or private browsing"
#~ msgstr "Använder publik eller privat surfning"
