#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-11-22 19:59+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/censorship-no/"
"user-manual/sv/>\n"
"Language: sv\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2.1-rc\n"

#: SUMMARY.md:block 1 (header)
msgid "Summary"
msgstr "Sammanfattning"

#: SUMMARY.md:block 2 (paragraph)
msgid "[Censorship.no!](README.md)"
msgstr "[Censorship.no!](README.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Introduction](intro/README.md)"
msgstr "[Introduktion](intro/README.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[What is Ceno Browser?](intro/ceno.md)"
msgstr "[Vad är Ceno-navigatorn?](intro/ceno.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[What is Ouinet?](intro/ouinet.md)"
msgstr "[Vad är Ouinet?](intro/ouinet.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Quick start guide](intro/qsg.md)"
msgstr ""

#: SUMMARY.md:block 3 (unordered list)
msgid "[Frequently Asked Questions](intro/faq.md)"
msgstr "[Frekvent Ställda Frågor](intro/faq.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Main concepts](concepts/README.md)"
msgstr "[Huvudkoncept](concepts/README.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[How does it work?](concepts/how.md)"
msgstr "[Hur fungerar det?](concepts/how.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Public vs. private browsing](concepts/public-private.md)"
msgstr "[Publik mot privat surfning](concepts/public-private.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Advantages of using Ceno/Ouinet](concepts/advantages.md)"
msgstr "[Fördelar med att använda Ceno/Ouinet](concepts/advantages.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Risks in using Ceno/Ouinet](concepts/risks.md)"
msgstr "[Risker med att använda Ceno/Ouinet](concepts/risks.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Using Ceno Browser](browser/README.md)"
msgstr "[Att testa navigatorn](browser/testing.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Installing Ceno](browser/install.md)"
msgstr "[Installera Ceno](browser/install.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Using public or private browsing](browser/public-private.md)"
msgstr "[Att använda publik eller privat surfning](browser/public-private.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Ceno settings](browser/settings.md)"
msgstr "[Ceno-inställningar](browser/settings.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Testing the Browser](browser/testing.md)"
msgstr "[Att testa navigatorn](browser/testing.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Helping others](browser/bridging.md)"
msgstr "[Hjälp andra](browser/bridging.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Troubleshooting](browser/troubleshooting.md)"
msgstr "[Felsökning](browser/troubleshooting.md)"

#: SUMMARY.md:block 3 (unordered list)
msgid "[Annex: The Ouinet client front-end](client/front-end.md)"
msgstr "[Annex: Framänden för Oiunet-klienten](client/front-end.md)"
